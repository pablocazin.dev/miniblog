const express = require('express')
const app = express()
const path = require('path')
const crypto = require('crypto')

// pour utiliser le module de lecture / ecriture de fichier
const fs = require('fs')

// lire le contenu de la requete POST
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

app.use(express.static('./public'));
app.use(express.static('./DATA'));

/*****************************************************************/

app.get('/blog/', function (req, res) {
    var options = {
        root: path.join(__dirname)
    };
    res.sendFile('public/index.html', options, function (err) {
        if (err) {
            console.log("error sending index.html" + err)
        } else {
            console.log('Sent:', 'index.html');
        }
    });
});

app.post('/blog/', function (req, res) {

    if (req.body.content !== "" && req.body.secret !== "") {

        const content = req.body.content
        const secret = req.body.secret
        const fichier = fs.readFileSync('./DATA/password.txt', { encoding: 'utf8', flag: 'r' })
        const hash = crypto.createHash('sha256').update(secret).digest('base64')

        if (hash === fichier.replace(/(\r\n|\n|\r)/gm, "")) {
            let date = new Date().toString()
            let mois = new Date().getMonth() + 1
            if (mois < 10) { mois = "0" + mois }
            let jour = date.slice(8, 10)
            let annee = date.slice(11, 15)
            let heure = date.slice(16, 18)
            let minute = date.slice(19, 21)
            let seconde = date.slice(22, 24)
            let fileName = annee + mois + jour + heure + minute + seconde 

            const fs = require('fs');
            fs.writeFile(
                `${fileName}.json`, 
                `{"url": "http://localhost/node/DATA/${fileName}.json","content": "${content.toString()}","date": "${fileName}"}`, 
                function (err) {
                    if (err) throw err;
                    console.log('Fichier créé !');
            });

            res.status(200).send(fileName)
            console.log("content: " + content + "\nsecret: " + secret)
        } else {
            res.status(403).send(`le secret ne correspond pas -> ${secret}, HASH ->${hash} FICHIER -> ${fichier}`)
        }
    } else {
        res.status(403).send('contenu ou secret manquant.')
    }

    // authentifier la requete
    // hasher le secret
    // verifier si le secret hashé correspond au hash stocké
    // si le secret correspond, créer un nouveau fichier json pour le post
    // ecrire dans le fichier json l'ur du post, son contenu, sa date
    // ajouter le post dans la liste des posts.json
    // renvoyer /blog/index.html
})

app.get('/DATA/posts.json', function (req, res) {
    // renvoyer la liste des url de tous les posts
})

app.get('/DATA/:id.json', function (req, res) {
    // renvoie un post au format json
})

// express s'ouvre automatiquement sur le port 3000
app.listen(3000)

//.toString().replaceAll("-", "").replaceAll(":", "").replaceAll(".", "").replaceAll("Z", "").replaceAll("T", "")



